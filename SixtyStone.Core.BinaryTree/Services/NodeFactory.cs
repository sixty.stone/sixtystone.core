﻿using SixtyStone.Core.BinaryTree.Models;

namespace SixtyStone.Core.BinaryTree.Services
{
    public interface INodeFactory<T>
    {
        IBinaryTreeNode<T> GetBinaryTreeNode(T value);
    }

    public class NodeFactory<T> : INodeFactory<T>
    {
        public IBinaryTreeNode<T> GetBinaryTreeNode(T value)
        {
            return new BinaryTreeNode<T>(value);
        }
    }
}
