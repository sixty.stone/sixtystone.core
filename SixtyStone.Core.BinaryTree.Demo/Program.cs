﻿using System;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using Microsoft.Extensions.DependencyInjection;
using SixtyStone.Core.BinaryTree.Models;
using SixtyStone.Core.BinaryTree.Services;

namespace SixtyStone.Core.BinaryTree.Demo
{
    class Program
    {
        static void Main(string[] args)
        {
            var container = new ServiceCollection();

            container.AddSingleton<ITreeHelper<int>, TreeHelper<int>>();
            container.AddSingleton<IStringificationHelper<int>, StringificationHelper<int>>();
            container.AddSingleton<INodeFactory<int>, NodeFactory<int>>();

            var resolver = container.BuildServiceProvider();
            var builder = resolver.GetService<ITreeHelper<int>>();
            var stringifier = resolver.GetService<IStringificationHelper<int>>();
            var regex = new Regex("^(-?[\\d]+,)+-?[\\d]+$");
            var input = string.Empty;

            while (input != "exit")
            {
                Console.Write($" Enter a positive integer n to create a complete binary tree with n nodes,{Environment.NewLine}  or a comma-delimited list of integers to create a binary search tree.{Environment.NewLine} For best results, avoid generating trees of more than 5 levels{Environment.NewLine}  (doing so will result in distorted images on most screens).{Environment.NewLine} Enter \"exit\" to quit: ");

                input = Console.ReadLine();

                if (int.TryParse(input, out var size))
                {
                    var nodeValues = new List<int>();

                    for (var i = 1; i <= size; i++)
                    {
                        nodeValues.Add(i);
                    }

                    var tree = builder.BuildTree(nodeValues, BinaryTreeType.Complete);

                    Console.WriteLine(stringifier.Stringify(tree));

                    Console.WriteLine();
                }
                else if (regex.IsMatch(input))
                {
                    var split = input.Split(',');
                    var nodeValues = new List<int>();

                    foreach (var strNodeValue in split)
                    {
                        if (int.TryParse(strNodeValue, out int iNodeValue))
                        {
                            nodeValues.Add(iNodeValue);
                        }
                    }

                    var tree = builder.BuildTree(nodeValues, BinaryTreeType.Search);

                    Console.WriteLine(stringifier.Stringify(tree));

                    Console.WriteLine();
                }
                else if (input.ToLower() != "exit")
                {
                    Console.WriteLine();

                    Console.WriteLine("Please enter a valid input, e.g. 15 or 6,1,4,9,7,21");

                    Console.WriteLine();
                }
            }
        }
    }
}
