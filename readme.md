# SixtyStone.Core.BinaryTree

Builds and prints a binary tree.

![demo1](/SixtyStone.Core.BinaryTree.Demo/demo-search.jpg?raw=true "Binary Search Tree Demo")

![demo2](/SixtyStone.Core.BinaryTree.Demo/demo-complete.jpg?raw=true "Complete Binary Tree Demo")
